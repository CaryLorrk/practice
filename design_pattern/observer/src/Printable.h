#ifndef PRINTABLE_H
#define PRINTABLE_H

#include <iostream>
#include <string>
#include <deque>

#include "Printer.h"

class Printable;
static std::ostream& operator<<(std::ostream &os, const Printable &obj);

class Printable {
    friend std::ostream& operator<<(std::ostream &os, const Printable &obj);

public:
    void notify () const {
        for (auto &printer: listeners) {
            printer->notify(*this);
        }
    }

    void registerPrinter(const Printer *printer) {
        listeners.push_back(ConstPrinterPtr(printer));
    }

    virtual std::string toString() const = 0;

protected:
    std::deque<ConstPrinterPtr> listeners;
    

};

static std::ostream& operator<<(std::ostream &os, const Printable &obj) {
    os << obj.toString();
    return os;
}


#endif


