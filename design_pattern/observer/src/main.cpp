#include <iostream>

#include "Integer.h"
#include "StarPrinter.h"
#include "SharpPrinter.h"

int main()
{
    Integer i(1);
    i.registerPrinter(new StarPrinter());
    i.registerPrinter(new SharpPrinter());
    i.notify();
    i.setValue(2);
}

