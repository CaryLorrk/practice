#ifndef STARPRINTER_H
#define STARPRINTER_H

#include <iostream>
#include "Printer.h"

class StarPrinter : public Printer {
public:
    void notify(const Printable &obj) const {
        std::cout << "*" << obj << "*" << std::endl;        
    }
};

#endif

