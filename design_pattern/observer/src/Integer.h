#ifndef INTEGER_H
#define INTEGER_H

#include "Printable.h"

class Integer : public Printable {
public:
    Integer(const int i) : value(i) {}

    void setValue(const int i) {
        value = i;
        notify();
    }

    std::string toString() const {
        return std::to_string(value);
    }
    
private:
    int value;
};

#endif

