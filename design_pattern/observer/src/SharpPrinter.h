#ifndef SHARPPRINTER_H
#define SHARPPRINTER_H

#include <iostream>
#include "Printer.h"

class SharpPrinter : public Printer {
    void notify(const Printable &obj) const {
        std::cout << "#" << obj << "#" << std::endl;        
    }
};

#endif

