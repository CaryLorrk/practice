#ifndef Printer_H
#define Printer_H

#include <memory>

class Printable;
class Printer {
public:
    virtual ~Printer() = default;
    virtual void notify(const Printable &obj) const = 0;
};

typedef std::shared_ptr<Printer> PrinterPtr;
typedef std::shared_ptr<const Printer> ConstPrinterPtr;
#endif

