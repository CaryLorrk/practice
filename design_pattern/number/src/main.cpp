/* Copyright (C)
 * 2014 - CaryLorrk
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Number
#include <boost/test/unit_test.hpp>
#include "Number.h"

struct Numbers{
    Number r1;
    Number r2;
    Number c1;
    Number c2;
    Numbers():
        r1(1), r2(-2), c1(3, 4), c2(-5, -6) {}
};

BOOST_FIXTURE_TEST_SUITE(Operation, Numbers)

BOOST_AUTO_TEST_CASE(realAddReal) {
    BOOST_CHECK_CLOSE((r1+r2), Number(-1), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realSubReal) {
    BOOST_CHECK_CLOSE(r1-r2, Number(3), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realMulReal) {
    BOOST_CHECK_CLOSE(r1*r2, Number(-2), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realDivReal) {
    BOOST_CHECK_CLOSE(r1/r2, Number(-0.5f), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realAddComplex) {
    BOOST_CHECK_CLOSE(r1+c1, Number(4, 4), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realSubComplex) {
    BOOST_CHECK_CLOSE(r1-c1, Number(-2, -4), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realMulComplex) {
    BOOST_CHECK_CLOSE(r1*c1, Number(3, 4), Number(0.001));
}

BOOST_AUTO_TEST_CASE(realDivComplex) {
    BOOST_CHECK_CLOSE(r1/c1, Number(0.12, -0.16), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexAddReal) {
    BOOST_CHECK_CLOSE(c2+r2, Number(-7, -6), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexSubReal) {
    BOOST_CHECK_CLOSE(c2-r2, Number(-3, -6), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexMulReal) {
    BOOST_CHECK_CLOSE(c2*r2, Number(10, 12), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexDivReal) {
    BOOST_CHECK_CLOSE(c2/r2, Number(2.5, 3), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexAddComplex) {
    BOOST_CHECK_CLOSE(c1+c2, Number(-2, -2), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexSubComplex) {
    BOOST_CHECK_CLOSE(c1-c2, Number(8, 10), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexMulComplex) {
    BOOST_CHECK_CLOSE(c1*c2, Number(9, -38), Number(0.001));
}

BOOST_AUTO_TEST_CASE(complexDivComplex) {
    BOOST_CHECK_CLOSE(c1/c2, Number(-0.639344262,
                                    -0.0327868852),
                                    Number(0.001));
}

BOOST_AUTO_TEST_SUITE_END()
