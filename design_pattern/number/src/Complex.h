/* Copyright (C)
 * 2014 - CaryLorrk
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <iostream>
#include <sstream>
#include <string>
#include <cmath>
#include "NumberImpl.h"


struct Complex: NumberImpl, std::enable_shared_from_this<Complex> {
    double real;
    double image;

    Complex(double i, double j): real(i), image(j) {}
    ~Complex() = default;

    NumberImplPtr add(const NumberImplPtr &rhs) const;
    NumberImplPtr add(const RealPtr &lhs) const;
    NumberImplPtr add(const ComplexPtr &lhs) const;

    NumberImplPtr substract(const NumberImplPtr &rhs) const;
    NumberImplPtr substract(const RealPtr &lhs) const;
    NumberImplPtr substract(const ComplexPtr &lhs) const;

    NumberImplPtr multiply(const NumberImplPtr &rhs) const;
    NumberImplPtr multiply(const RealPtr &lhs) const;
    NumberImplPtr multiply(const ComplexPtr &lhs) const;

    NumberImplPtr divide(const NumberImplPtr &rhs) const;
    NumberImplPtr divide(const RealPtr &lhs) const;
    NumberImplPtr divide(const ComplexPtr &lhs) const;

    bool equal(const NumberImplPtr &rhs) const;
    bool equal(const RealPtr &lhs) const;
    bool equal(const ComplexPtr &lhs) const;

    bool less(const NumberImplPtr &rhs) const;
    bool less(const RealPtr &lhs) const;
    bool less(const ComplexPtr &lhs) const;

    ComplexPtr getConjugate() const {
        return ComplexPtr(new Complex(real, -image));
    }

    NumberImplPtr copy() const {
        return ComplexPtr(new Complex(*this));
    }

    double getAbsolute() const {
        return std::sqrt(real * real + image * image);
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "(" << real << "," << image << ")";
        return oss.str();
    }
};
#endif

