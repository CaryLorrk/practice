/* Copyright (C)
 * 2014 - CaryLorrk
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Complex.h"
#include "Real.h"

NumberImplPtr Complex::add(const NumberImplPtr &rhs) const {
    return rhs->add(shared_from_this());
}

NumberImplPtr Complex::add(const RealPtr &lhs) const {
    return ComplexPtr(new Complex(real + lhs->data, image));
}

NumberImplPtr Complex::add(const ComplexPtr &lhs) const {
    return ComplexPtr(new Complex(real + lhs->real, image + lhs->image));
}

NumberImplPtr Complex::substract(const NumberImplPtr &rhs) const {
    return rhs->substract(shared_from_this());
}
NumberImplPtr Complex::substract(const RealPtr &lhs) const {
    return ComplexPtr(new Complex(lhs->data - real, - image));
}
NumberImplPtr Complex::substract(const ComplexPtr &lhs) const {
    return ComplexPtr(new Complex(lhs->real - real, lhs->image - image));
}

NumberImplPtr Complex::multiply(const NumberImplPtr &rhs) const {
    return rhs->multiply(shared_from_this());
}
NumberImplPtr Complex::multiply(const RealPtr &lhs) const {
    return ComplexPtr(new Complex(lhs->data * real, lhs->data * image));
}
NumberImplPtr Complex::multiply(const ComplexPtr &lhs) const {
    return ComplexPtr(
            new Complex(
                lhs->real * real - lhs->image * image,
                lhs->real * image + lhs->image * real));
}

NumberImplPtr Complex::divide(const NumberImplPtr &rhs) const {
    return rhs->divide(shared_from_this());
}
NumberImplPtr Complex::divide(const RealPtr &lhs) const {
    ComplexPtr conjugate = getConjugate();
    ComplexPtr dividend(
            new Complex(
                lhs->data * conjugate->real,
                conjugate->image));
    double divisor = real * conjugate->real - image * conjugate->image;
    return ComplexPtr(
            new Complex(
                dividend->real / divisor,
                dividend->image / divisor));
}

NumberImplPtr Complex::divide(const ComplexPtr &lhs) const {
    ComplexPtr conjugate = getConjugate();
    ComplexPtr dividend(
            new Complex(
                lhs->real * conjugate->real - lhs->image * conjugate->image,
                lhs->real * conjugate->image + lhs->image * conjugate->real));
    double divisor = real * conjugate->real - image * conjugate->image;
    return ComplexPtr(
            new Complex(
                dividend->real / divisor,
                dividend->image / divisor));
}

bool Complex::equal(const NumberImplPtr &rhs) const {
    return rhs->equal(shared_from_this());
}
bool Complex::equal(const RealPtr &lhs) const {
    return lhs->data == real && real == 0;
}
bool Complex::equal(const ComplexPtr &lhs) const {
    return lhs->real == real && lhs->image == image;
}

bool Complex::less(const NumberImplPtr &rhs) const {
    return rhs->less(shared_from_this());
}
bool Complex::less(const RealPtr &lhs) const {
    return lhs->data <= getAbsolute();
}
bool Complex::less(const ComplexPtr &lhs) const {
    return lhs->getAbsolute() <= getAbsolute();
}
