/* Copyright (C)
 * 2014 - CaryLorrk
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef NUMBERIMPL_H
#define NUMBERIMPL_H

#include <memory>
#include <string>

struct NumberImpl;
typedef std::shared_ptr<const NumberImpl> NumberImplPtr;

struct Real;
typedef std::shared_ptr<const Real> RealPtr;

struct Complex;
typedef std::shared_ptr<const Complex> ComplexPtr;

struct NumberImpl {
    virtual ~NumberImpl() = default;

    virtual NumberImplPtr add(const NumberImplPtr &rhs) const = 0;
    virtual NumberImplPtr add(const RealPtr &lhs) const = 0;
    virtual NumberImplPtr add(const ComplexPtr &lhs) const = 0;

    virtual NumberImplPtr substract(const NumberImplPtr &rhs) const = 0;
    virtual NumberImplPtr substract(const RealPtr &lhs) const = 0;
    virtual NumberImplPtr substract(const ComplexPtr &lhs) const = 0;

    virtual NumberImplPtr multiply(const NumberImplPtr &rhs) const = 0;
    virtual NumberImplPtr multiply(const RealPtr &lhs) const = 0;
    virtual NumberImplPtr multiply(const ComplexPtr &lhs) const = 0;

    virtual NumberImplPtr divide(const NumberImplPtr &rhs) const = 0;
    virtual NumberImplPtr divide(const RealPtr &lhs) const = 0;
    virtual NumberImplPtr divide(const ComplexPtr &lhs) const = 0;

    virtual bool equal(const NumberImplPtr &rhs) const = 0;
    virtual bool equal(const RealPtr &lhs) const = 0;
    virtual bool equal(const ComplexPtr &lhs) const = 0;

    virtual bool less(const NumberImplPtr &rhs) const = 0;
    virtual bool less(const RealPtr &lhs) const = 0;
    virtual bool less(const ComplexPtr &lhs) const = 0;

    virtual NumberImplPtr copy() const = 0;
    virtual std::string toString() const = 0;
};
#endif

