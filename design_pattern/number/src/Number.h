/* Copyright (C)
 * 2014 - CaryLorrk
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef NUMBER_H_
#define NUMBER_H_

#include <iostream>
#include <boost/operators.hpp>

#include "NumberImpl.h"
#include "Real.h"
#include "Complex.h"

class Number :
        boost::addable<Number>,
        boost::subtractable<Number>,
        boost::multipliable<Number>,
        boost::dividable<Number>,
        boost::partially_ordered<Number> {
 public:
    Number(const double i = 0) : impl(RealPtr(new Real(i))) {}
    Number(const double i, double j) :
        impl(ComplexPtr(new Complex(i, j))) {}
    Number(const NumberImplPtr i) : impl(i) {}
    Number(const Number &i) : impl(i.impl->copy()) {}
    Number(Number &&i) : impl(i.impl) {}

    Number operator=(const Number &i) {
        impl = i.impl->copy();
        return *this;
    }

    Number& operator+=(const Number &rhs) {
        impl = impl->add(rhs.impl);
        return *this;
    }

    Number& operator-=(const Number &rhs) {
        impl = impl->substract(rhs.impl);
        return *this;
    }

    Number& operator*=(const Number &rhs) {
        impl = impl->multiply(rhs.impl);
        return *this;
    }

    Number& operator/=(const Number &rhs) {
        impl = impl->divide(rhs.impl);
        return *this;
    }

    Number operator-() const {
        return Number(0) - *this;
    }

    bool operator==(const Number &rhs) const {
        return impl->equal(rhs.impl);
    }

    bool operator<(const Number &rhs) const {
         return impl->less(rhs.impl);
    }

    friend std::ostream& operator<<(std::ostream &os, const Number &i);

 private:
    NumberImplPtr impl;
};


#endif

