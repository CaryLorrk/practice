/* Copyright (C)
 * 2014 - CaryLorrk
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Real.h"
#include "Complex.h"

NumberImplPtr Real::add(const NumberImplPtr &rhs) const {
    return rhs->add(shared_from_this());
}

NumberImplPtr Real::add(const RealPtr &lhs) const {
    return RealPtr(new Real(data + lhs->data));
}

NumberImplPtr Real::add(const ComplexPtr &lhs) const {
    return ComplexPtr(new Complex(data + lhs->real, lhs->image));
}

NumberImplPtr Real::substract(const NumberImplPtr &rhs) const {
    return rhs->substract(shared_from_this());
}
NumberImplPtr Real::substract(const RealPtr &lhs) const {
    return RealPtr(new Real(lhs->data - data));
}
NumberImplPtr Real::substract(const ComplexPtr &lhs) const {
    return ComplexPtr(new Complex(lhs->real - data, lhs->image));
}

NumberImplPtr Real::multiply(const NumberImplPtr &rhs) const {
    return rhs->multiply(shared_from_this());
}

NumberImplPtr Real::multiply(const RealPtr &lhs) const {
    return RealPtr(new Real(lhs->data * data));
}

NumberImplPtr Real::multiply(const ComplexPtr &lhs) const {
    return ComplexPtr(new Complex(lhs->real * data, lhs->image * data));
}

NumberImplPtr Real::divide(const NumberImplPtr &rhs) const {
    return rhs->divide(shared_from_this());
}

NumberImplPtr Real::divide(const RealPtr &lhs) const {
    return RealPtr(new Real(lhs->data / data));
}

NumberImplPtr Real::divide(const ComplexPtr &lhs) const {
    return ComplexPtr(new Complex(lhs->real / data, lhs->image / data));
}

bool Real::equal(const NumberImplPtr &rhs) const {
    return rhs->equal(shared_from_this());
}
bool Real::equal(const RealPtr &lhs) const {
    return lhs->data == data;
}
bool Real::equal(const ComplexPtr &lhs) const {
    return lhs->real == data && lhs->image == 0;
}

bool Real::less(const NumberImplPtr &rhs) const {
     return rhs->less(shared_from_this());
}
bool Real::less(const RealPtr &lhs) const {
    return lhs->data <= data;
}
bool Real::less(const ComplexPtr &lhs) const {
    return lhs->getAbsolute() <= data;
}
