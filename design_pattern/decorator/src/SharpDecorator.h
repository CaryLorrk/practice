#ifndef STARDECORATOR_H
#define STARDECORATOR_H

#include <iostream>
#include "Decorator.h"

class SharpDecorator: public Decorator
{
public:
    using Decorator::Decorator;
    void print() const {
        std::cout << '#';
        Decorator::print();
        std::cout << '#';
    }
};

#endif

