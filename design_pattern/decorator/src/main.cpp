#include <iostream>
#include <memory>

#include "Display.h"
#include "IntDisplay.h"
#include "SharpDecorator.h"
#include "StarDecorator.h"

using namespace std;

typedef shared_ptr<Display> DisplayPtr;
int main()
{
    DisplayPtr display1(new IntDisplay(1));
    DisplayPtr display2(DisplayPtr(new SharpDecorator(DisplayPtr(new StarDecorator(DisplayPtr(new IntDisplay(2)))))));

    display1->print();
    cout << endl;
    display2->print();
    cout << endl;

    return 0;
}

