#ifndef INTDISPLAY_H
#define INTDISPLAY_H

#include <iostream>
#include "Display.h"

class IntDisplay: public Display
{
public:
    IntDisplay (const int i): data(i) {}
    virtual void print() const {
        std::cout << data;
    }

private:
    const int data;
};

#endif

