#ifndef DECORATOR_H
#define DECORATOR_H

#include <memory>
#include "Display.h"

class Decorator: public Display
{
public:
    Decorator(const std::shared_ptr<Display> i): component(i) {}
    
protected:
    virtual void print() const {
        component->print();
    }

private:
    const std::shared_ptr<Display> component;
};

#endif

