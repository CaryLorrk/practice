#ifndef DISPLAY_H
#define DISPLAY_H

#include <iostream>

class Display
{
public:
    virtual ~Display() = default;
    virtual void print() const = 0;
};

#endif

