#ifndef SHARPDECORATOR_H
#define SHARPDECORATOR_H

#include <iostream>
#include "Decorator.h"

class StarDecorator: public Decorator
{
public:
    using Decorator::Decorator;
    void print() const {
        std::cout << '*';
        Decorator::print();
        std::cout << '*';
    }
};

#endif

