#include <iostream>
#include <utility>
using namespace std;

/* input */
const int v = 10;
const int s = 1;
const int S = 2;
const int L = 5;
const int nb_rail = 3;
const int rail[nb_rail][L] =
        {
            {0,0,0,0,0},
            {3,3,3,1,1},
            {1,1,1,3,3}
        };

/* intermediate */
pair<int,int> path[nb_rail][L];    

/* output */
int solution[L];

pair<int,int> compute(int nr, int nl)
{
    pair<int,int> max = make_pair(nr, path[nr][nl-1].second + rail[nr][nl]);
    for (int r = 0; r < nb_rail; ++r)
    {
        if (r == nr)
            continue;

        int speed = path[r][nl-1].second + rail[nr][nl] - (s+S);
        if (speed > max.second) {
            max.first = r;
            max.second = speed;
        }
    }
    return max;
}

void find_path()
{
    /* find max speed and last rail */
    int speed = path[0][L-1].second;
    solution[L-1] = 0;
    for (int r = 1; r < nb_rail; ++r) {
        if (path[r][L-1].second > solution[L-1]) {
            speed = path[r][L-1].second;
            solution[L-1] = r;
        }
    }

    /* back trace */
    for (int nl = L-2; nl >= 0; --nl) {
        solution[nl] = path[solution[nl+1]][nl+1].first;
    }
}


int main()
{
    for (int nr = 0; nr < nb_rail; ++nr)
        path[nr][0] = make_pair(-1, rail[nr][0]);

    for (int nl = 1; nl < L; ++nl) {
        for (int nr = 0; nr < nb_rail; ++nr) {
            path[nr][nl] = compute(nr,nl); 
        }
    }

    find_path();

    cout << "final speed: " << path[solution[L-1]][L-1].second + v - (s+S) << endl;
    cout << "path: " << endl;
    for (int l = 0; l < L; ++l)
        cout << solution[l] << " ";
    cout << endl;
}
