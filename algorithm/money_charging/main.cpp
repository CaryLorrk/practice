/**
 * input format:
 * 0 < M < 10^5
 * 0 < N < 10^5
 * 0 < V < 10^5
 * M
 * N
 * V_1
 * V_2
 * ...
 * V_N
 * 
 */
#include <cstdio>
#include <vector>
using namespace std;

int M;
int N;
int V[100000];
bool exist[100000];
vector<int> combination[100000];
int count[100000];

int main() {
    scanf("%d", &M);
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        scanf("%d", &V[i]);
    }
    
    exist[0] = true;
    for(int m = 1; m <= M; ++m) {
        for (int n = 0; n < N && n <= m; ++n) {
            int prev = m - V[n];
            if (exist[prev]) {
                exist[m] = true;
                combination[m] = combination[prev];
                combination[m].push_back(n);
                break;
            }
        }
    }

    for (int i = M; i >= 0; --i) {
        if (exist[i]) {
            printf("%d\n", i);
            for(int item : combination[i]) {
                ++count[item];
            }
            for (int i = 0; i < N; ++i) {
                count[i] && printf("%d * %d\n", V[i], count[i]);
            }
        break;
        }
    }

    return 0;
}
