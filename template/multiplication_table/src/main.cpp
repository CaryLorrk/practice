#include <iostream>
using namespace std;

template <int m, int n>
struct rightside
{
    static void output()
    {
        rightside<m,n-1>::output();
        cout << m << " x " << n << " = " << m*n << endl;
    }
};

template <int m>
struct rightside<m,0>
{
    static void output()
    {
        return ;
    }
};

template <int m, int n>
struct leftside
{
    static void output()
    {
        leftside<m-1,n>::output();
        rightside<m,n>::output();
    }
};

template <int n>
struct leftside<0,n>
{
    static void output()
    {
        return;
    }
};

int main()
{
    leftside<9,9>::output();
    return 0;
}
