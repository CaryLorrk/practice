#include <iostream>
template<int SIZE>
void print(int (&arr)[SIZE])
{
    std::cout << sizeof(arr);
}

int main()
{
    int arr[] = {1,2,3,4,5};

    print(arr);
}
