#include <iostream>
#include <string>
#include <vector>
#include <set>

namespace FileSystem {
    struct Info {
        std::string path;
    };
}

#define STATIC_ASSERT(cond, msg) \
typedef char static_assert_##msg[2*(!!cond)-1];

#define CREATE_MEMBER_DETECTOR(member) \
template<typename T> \
class detector_of_##member { \
private: \
    typedef char Yes[1]; \
    typedef char No[2]; \
    struct Base {char member;}; \
    struct Derived: T, Base {}; \
    \
    template<typename PointerToMember, PointerToMember> \
    struct Check {}; \
    \
    template<typename D> \
    static Yes& result(...); \
    template<typename D> \
    static No& result(Check<char Base::*, &D::member>*); \
    \
public: \
    enum {value = sizeof(result<Derived>(0)) == sizeof(Yes)}; \
};

CREATE_MEMBER_DETECTOR(push_back)
CREATE_MEMBER_DETECTOR(path)

typedef std::set<FileSystem::Info> X;
typedef std::vector<std::string> Y;
typedef std::vector<FileSystem::Info> Z;
int main()
{
    //STATIC_ASSERT(detector_of_push_back<X>::value, X_no_push_back);
    STATIC_ASSERT(detector_of_push_back<Y>::value, Y_no_push_back);
    //STATIC_ASSERT(detector_of_path<Y::value_type>::value, Y_no_path);
    STATIC_ASSERT(detector_of_path<Z::value_type>::value, Z_no_path);
}
