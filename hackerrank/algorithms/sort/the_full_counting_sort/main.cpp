#include <iostream>
#include <cstdio>
#include <tuple>
#include <string>
#include <vector>
using namespace std;

typedef tuple<int, string, int> Item;
int n;
Item ar[1000000];
vector<Item> cnt[100];


int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        int tmp_int;
        string tmp_str;
        cin >> tmp_int >> tmp_str;
        ar[i] = make_tuple(tmp_int, tmp_str, i);
    }


    for (int i = 0; i < n; ++i) {
        cnt[get<0>(ar[i])].push_back(ar[i]);
    }

    for (int i = 0; i < 100; ++i) {
        for (auto &item : cnt[i]) {
            if (get<2>(item) < n/2)
                printf("%c ", '-');
            else 
                printf("%s ", get<1>(item).c_str());
        }
    }
    printf("\n");
}
