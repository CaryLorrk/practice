#include <cstdio>

int N;
int AR[1000];
int main() {
    scanf("%d", &N);
    for (int n = 0; n < N; ++n) {
        scanf("%d", &AR[n]);
    }

    int left[1000];
    int nLeft = 0;
    int right[1000];
    int nRight = 0;
    for (int i = 1; i < N; ++i) {
        if (AR[i] < AR[0]) {
            left[nLeft++] = AR[i];
        }
        else {
            right[nRight++] = AR[i];
        }
    }

    for (int i = 0; i < nLeft; ++i) {
        printf("%d ", left[i]);
    }
    printf("%d ", AR[0]);
    for (int i = 0; i < nRight; ++i) {
        printf("%d ", right[i]);
    }

    printf("\n");

    return 0;
}
