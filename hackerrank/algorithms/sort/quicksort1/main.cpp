#include <cstdio>
using namespace std;

int n;
int ar[1000];
int p;
int main () {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &ar[i]);
    }
    p = ar[0];

    int idx[2] = {0};
    int ar_partition[2][1000];

    for (int i = 0; i < n; ++i) {
        int c = ar[i] < p ? 0 : 1;
        ar_partition[c][idx[c]++] = ar[i];
    }

    for (int c = 0; c < 2; ++c) {
        for (int i = 0; i < idx[c]; ++i) {
            printf("%d ", ar_partition[c][i]);
        }
    }

    printf("\n");


    return 0;
}
