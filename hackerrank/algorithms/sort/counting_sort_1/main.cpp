#include <cstdio>

int n;
int ar[1000000];
int cnt[100];
int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &ar[i]);
    }

    for (int i = 0; i < n; ++i) {
        cnt[ar[i]]++;
    }

    for (int i = 0; i < 100; ++i) {
        printf("%d ", cnt[i]);
    }
    printf("\n");
    return 0;
}
