#include <cstdio>

void print_arr(int *begin, int *end) {
    while (begin != end) {
        printf("%d ", *begin);
        ++begin;
    }
    printf("\n");
}

int s;
int ar[1000];
int main() {
    int v;
    int i;
    scanf("%d", &s);
    for (i = 0; i < s; ++i) {
        scanf("%d", &ar[i]);
    }
    
    v = ar[s-1];
    for (i = 0; i < s - 1; ++i) {
        if (ar[i] > v)
            break;
    }

    for (int j = s - 1; j > i; --j) {
        ar[j] = ar[j - 1];
        print_arr(ar, ar + s);
    }
    ar[i] = v;
    print_arr(ar, ar + s);
    

    return 0;
}
