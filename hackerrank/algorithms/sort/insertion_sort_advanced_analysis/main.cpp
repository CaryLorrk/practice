#include <cstdio>
#include <algorithm>
using namespace std;

struct Element {
    int num;
    int pos;
    bool operator< (const Element &rhs) const {
        return num < rhs.num || 
            (num == rhs.num && pos < rhs.pos);
    }
};

int T;
int N;
Element a[100000];
int main() {
    scanf("%d", &T); 
    for (int t = 0; t < T; ++t) {
        long count = 0;
        scanf("%d", &N);
        for (int n = 0; n < N; ++n) {
            scanf("%d", &a[n].num);
            a[n].pos = n;
        }
    
        stable_sort(a, a + N);

        for (int i = 0; i < N; ++i) {
            printf("%d %d %d\n", a[i].num, a[i].pos, i);
            if (a[i].pos < i) {
                count += i - a[i].pos;
            }
        }

        printf("%ld\n", count);
    }
    
    return 0;
}
