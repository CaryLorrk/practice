#include <cstdio>
#include <iterator>
using namespace std;

int n;
int ar[1000];


void partition(int * begin, int * end) {
    if (distance(begin, end) <= 1) {
        return;
    }

    int p = *begin;
    int idx[2] = {0};
    int ar_partition[2][1000];
    int cnt_p = 0;

    for (auto it = begin; it != end; ++it) {
        if (*it == p) {
            ++cnt_p;
            continue;
        }

        int c = *it < p ? 0 : 1;
        ar_partition[c][idx[c]++] = *it;
    }

    partition(ar_partition[0], ar_partition[0] + idx[0]);
    partition(ar_partition[1], ar_partition[1] + idx[1]);

    int *it = begin;
    for (int i = 0; i < idx[0]; ++i) {
        *(it++) = ar_partition[0][i];
    }

    for (int i = 0; i < cnt_p; ++i) {
        *(it++) = p;
    }

    for (int i = 0; i < idx[1]; ++i) {
        *(it++) = ar_partition[1][i];
    }

    for (int *it = begin; it != end; ++it) {
        printf("%d ", *it);        
    }
    printf("\n");
} 

int main () {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &ar[i]);
    }

    partition(ar, ar + n);

    return 0;
}
