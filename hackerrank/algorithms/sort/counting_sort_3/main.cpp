#include <iostream>
#include <cstdio>
#include <tuple>
#include <string>
#include <vector>
using namespace std;

typedef tuple<int, string> Item;
int n;
Item ar[1000000];
vector<Item> cnt[100];


int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        int tmp_int;
        string tmp_str;
        cin >> tmp_int >> tmp_str;
        ar[i] = make_tuple(tmp_int, tmp_str);
    }


    for (int i = 0; i < n; ++i) {
        cnt[get<0>(ar[i])].push_back(ar[i]);
    }

    int acc = 0;
    for (int i = 0; i < 100; ++i) {
        acc += cnt[i].size();
        printf("%d ", acc);
    }
    printf("\n");
}
