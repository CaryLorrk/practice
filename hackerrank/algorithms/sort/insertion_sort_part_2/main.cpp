#include <cstdio>

void print_arr(int *begin, int *end) {
    while (begin != end) {
        printf("%d ", *begin);
        ++begin;
    }
    printf("\n");
}

int s;
int ar[1000];
int main() {
    int v;
    int i;
    scanf("%d", &s);
    for (i = 0; i < s; ++i) {
        scanf("%d", &ar[i]);
    }
    
    for (int k = 2; k <= s; ++k) {
        v = ar[k-1];
        for (i = 0; i < k - 1; ++i) {
            if (ar[i] > v)
                break;
        }

        for (int j = k - 1; j > i; --j) {
            ar[j] = ar[j - 1];
        }
        ar[i] = v;
        print_arr(ar, ar + s);
    }
    
    return 0;
}
