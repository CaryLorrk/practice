#include <cstdio>

int T;
int CONTEXT[3];
int main() {
    scanf("%d", &T);
    for(int t = 0; t < T; ++t) {
        scanf("%d%d%d", &CONTEXT[0], &CONTEXT[1], &CONTEXT[2]);
        int n = CONTEXT[0] / CONTEXT[1];
        int i = n;
        while(i >= CONTEXT[2]) {
            int j = i / CONTEXT[2];
            n += j;
            i = j + i % CONTEXT[2];
        }
        printf("%d\n", n);
    }
}
