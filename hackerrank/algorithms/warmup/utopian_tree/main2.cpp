#include <cstdio>
#include <algorithm>
using namespace std;

int T;
int Ns[10];
int results[61] = {1};

int main() {
    scanf("%d", &T);
    for (int t = 0; t < T; ++t) {
        scanf("%d", &Ns[t]);
    }

    auto max = *max_element(Ns, Ns + T);
    for (int n = 1; n <= max; ++n) {
        results[n] = n % 2 ? results[n - 1] * 2 : results[n - 1] + 1;
    }

    for (int t = 0; t < T; ++t) {
        printf("%d\n", results[Ns[t]]);
    }
    return 0;
}

