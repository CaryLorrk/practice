#include <cstdio>
#include <cstdlib>

int T;
int N[11];
int result[61] = {1};
int end = 1;
int main() {
    scanf("%d", &T);
    for (int i = 0; i < T; ++i) { 
        scanf("%d", &N[i]);
    }

    for (int i = 0; i < T; ++i) {
        if (N[i] < end)
            continue;
        for (int j = end; j <= N[i]; ++j) {
            result[j] = j % 2 ? result[j-1]*2 : result[j-1]+1;
        }
        end = N[i] + 1;
    }

    for (int i = 0; i < T; ++i)
        printf("%d\n", result[N[i]]);
    
    return 0;
}
