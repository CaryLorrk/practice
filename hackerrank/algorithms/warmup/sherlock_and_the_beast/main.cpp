#include <cstdio>

int T;
int main() {
    scanf("%d", &T);
    for(int t = 0; t < T; ++t) {
        int N;
        scanf("%d", &N);
        bool result = false;
        if(N % 3 == 0) {
            for (int i = 0; i < N; ++i)
                printf("5");
            printf("\n");
            continue;
        }
        int n3;
        for(n3 = 5; n3 <= N; n3+=5) {
            if ((N - n3) % 3 == 0) {
                result = true;
                break;
            }
        }
        if(result == false) {
            printf("-1\n");
            continue;
        }
        for(int i = 0; i < N - n3; ++i)
            printf("5");
        for(int i = 0; i < n3; ++i)
            printf("3");
        printf("\n");
    }
}
