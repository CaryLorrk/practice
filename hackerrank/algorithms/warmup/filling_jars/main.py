#!/usr/bin/env python
# encoding: utf-8


def main():
    (N, M) = [int(i) for i in input().split()]
    result = 0
    for n in range(M):
        (a, b, k) = [int(i) for i in input().split()]
        result += (b - a + 1) * k

    print (int(result / N))


if __name__ == '__main__':
    main()
