#include <cstdio>

int T;
long long K[10];
long long result[10];
int main() {
    scanf("%d", &T);
    for(int t = 0; t < T; ++t) {
        scanf("%lld", &K[t]);
    }


    for(int t = 0; t < T; ++t)
        result[t] = K[t] % 2 ? (K[t] / 2 + 1) * (K[t] / 2) : K[t] / 2 * K[t] / 2;

    for(int t = 0; t < T; ++t)
        printf("%lld\n", result[t]);
}
