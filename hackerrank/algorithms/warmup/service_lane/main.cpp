#include <cstdio>
#include <algorithm>

int N;
int T;
int WIDTH[100000];
int idx[1000][2];
int result[1000];
int main() {
    scanf("%d", &N);        
    scanf("%d", &T);
    for(int n = 0; n < N; ++n) {
        scanf("%d", &WIDTH[n]);
    }
    for(int t = 0; t < T; ++t) {
        scanf("%d%d", &idx[t][0], &idx[t][1]);
    }
    for(int t = 0; t < T; ++t)
        result[t] = *std::min_element(&WIDTH[idx[t][0]], &WIDTH[idx[t][1]+1]);

    for(int t = 0; t < T; ++t)
        printf("%d\n", result[t]);

}
