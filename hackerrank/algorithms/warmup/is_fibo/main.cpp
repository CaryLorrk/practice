#include <cstdio>
#include <vector>
#include <algorithm>
using std::sort;

struct Number {
    long number;
    long pos;
    bool operator<(const Number &rhs) const {
        return number < rhs.number;
    }
};

int T;
Number N[10000];
int result[10000];
const char *str[2] = {"IsNotFibo", "IsFibo"};
long prev, now;
int main() {
    scanf("%d", &T);
    for (int i = 0; i < T; ++i) {
        N[i].pos = i;
        scanf("%ld", &N[i].number);
    }

    sort(N, N+T);

    prev = 0;
    now = 1;
    for (int i = 0; i < T; ++i) {
        while(now < N[i].number) {
            long next = prev + now;
            prev = now;
            now = next;
        }
        if (now == N[i].number)
            result[N[i].pos] = 1;
    }
    
    for (int i = 0; i < T; ++i) {
        printf("%s\n", str[result[i]]);
    }
    return 0;
}
