#include <cstdio>
#include <algorithm>
using std::sort;

int N, K;
long X[100000];
long minDiff;
int main()
{
    scanf("%d %d", &N, &K);
    for (int i = 0; i < N; ++i) {
        scanf("%ld", &X[i]);
    }
    
    sort(X, X+N);
    minDiff = X[K-1] - X[0];
    for (int i = 1; i < N - (K-1); ++i) {
        long diff = X[i+(K-1)] - X[i];
        if (diff < minDiff) {
            minDiff = diff;
        }
    }

    printf("%ld\n", minDiff);
    
    return 0;
}
