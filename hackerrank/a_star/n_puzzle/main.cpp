#include <cstdio>
#include <string>
#include <queue>
#include <vector>
#include <tuple>
#include <cmath>
#include <algorithm>
#include <unordered_map>
using namespace std;

int offset[4][2] = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};
struct State {
    enum Direction {
        UP    = 0,
        LEFT  = 1,
        RIGHT = 2,
        DOWN  = 3
    };
    static int k;
    int grid[5][5];
    int zero_row;
    int zero_col;
    Direction movement;
    int distance;

    static State& end() {
        static State singleton;
        singleton.distance = -1;
        return singleton;
    }

    bool operator==(const State &rhs) const {
        for (int r = 0; r < k; ++r) {
            for (int c = 0; c < k; ++c) {
                if (grid[r][c] != rhs.grid[r][c])
                    return false;
            }
        }
        return true;
    }

    int huristic() const {
        int h = 0;
        for (int r = 0; r < k; ++r) {
            for (int c = 0; c < k; ++c) {
                int num = grid[r][c];
                int right_row = num / k;
                int right_col = num % k;

                h += abs(right_row - r) + abs(right_col - c);
            }
        }
        return h;
    }

    bool move(Direction direction) {
        int next_row = zero_row + offset[direction][0];
        int next_col = zero_col + offset[direction][1];
        if (next_row < 0 && next_row >= k &&
            next_col < 0 && next_col >= k)
            return false;

        swap(grid[zero_row][zero_col], grid[next_row][next_col]);
        zero_row = next_row;
        zero_col = next_col;
        movement = direction;
        return true;
    }

    const char* toGridString() const {
        string tmp;
        for (int r = 0; r < k; ++r) {
            for (int c = 0; c < k; ++c) {
                tmp += to_string(grid[r][c]) + ' ';
            }
            tmp += '\n';
        }
        return tmp.c_str();
    }

    const char* toMovementString() const {
        static const char* text[4] =
        {"UP", "LEFT", "RIGHT", "DOWN"};
        return text[(int)movement];
    }
};

namespace std{
    template<>
    struct hash<State> {
        size_t operator()(const State &s) const {
            size_t hash_value = 0;
            hash<int> hasher;
            for (int r = 0; r < State::k; ++r) {
                for (int c = 0; c < State::k; ++c) {
                    size_t seed = r * State::k + c;
                    hash_value ^= 
                        hasher(s.grid[r][c]) + 
                        0x9e3779b9 + (seed << 6) + (seed >> 2);
                }
            }
            return hash_value;
        }
    };
}

int State::k = -1;
unordered_map<State, State> parentMap;
struct Cmp {
    bool operator()(const State &lhs, const State &rhs) {
        return lhs.distance + lhs.huristic() >
            rhs.distance + rhs.huristic();
    }
};
priority_queue<State, vector<State>, Cmp> pqueue;

int main() {
    State state;
    State finish;
    scanf("%d\n", &State::k);
    state.distance = 0;
    for (int r = 0; r < State::k; ++r) {
        for (int c = 0; c < State::k; ++c) {
            finish.grid[r][c] = r * State::k + c;
            scanf("%d\n", &state.grid[r][c]);
            if (state.grid[r][c] == 0) {
                state.zero_row = r;
                state.zero_col = c;
            }
        }
    }

    parentMap[state] = State::end();
    while(1) {
        if (state == finish) {
            vector<State> path;
            while (state.distance != 0) {
                path.push_back(state);
                state = parentMap[state];
            }

            printf("%lu\n", path.size());
            for (auto it = path.crbegin(); it != path.crend(); ++it) {
                printf("%s\n", it->toMovementString());
            }
            break;
        }

        for (int i = 0; i < 4; ++i) {
            State neighbor = state;
            bool valid = neighbor.move((State::Direction)i);
            if (valid) {
                neighbor.distance += 1;
                auto it = parentMap.find(neighbor);
                if (it == parentMap.end()) {
                    parentMap[neighbor] = state;
                    pqueue.push(neighbor);
                }
            }
        }

        state = pqueue.top();
        pqueue.pop();
    }

    return 0;
}
