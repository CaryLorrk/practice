#include <iostream>
#include <cstdio>
#include <vector>
#include <deque>
#include <unordered_map>
#include <string>
using namespace std;

struct Point {
    int row;
    int col;

    static Point &end() {
        static Point temp;
        return temp;
    }

    Point (const int r = -1, 
           const int c = -1):
        row(r), col(c) {}

    bool operator==(const Point &rhs) const {
        return row == rhs.row && col == rhs.col;
    }

    const char* toString() const {
        return (to_string(row) + " " + to_string(col)).c_str();
    }
};

namespace std {
  template <>
  struct hash<Point>
  {
    size_t operator()(const Point& p) const
    {
      return ((hash<int>()(p.row)) ^ 
              (hash<int>()(p.col) << 1));
    }
  };
}

Point pacman, food;
int nb_row, nb_col;
string grid[40];
deque<Point> queue;
vector<Point> tree;
unordered_map<Point, Point> parentMap;

int main() {
    scanf("%d %d\n", &pacman.row, &pacman.col);
    scanf("%d %d\n", &food.row, &food.col);
    scanf("%d %d\n", &nb_row, &nb_col);
    for (int r = 0; r < nb_row; ++r) {
        getline(cin, grid[r]);
    }

    parentMap[pacman] = Point::end();
    grid[pacman.row][pacman.col] = '%';
    while(1) {
        tree.push_back(pacman);
        if (pacman == food) {
            printf("%lu\n", tree.size());
            for (auto p: tree) {
                printf("%s\n", p.toString());
            }

            vector<Point> path;
            Point &end = Point::end();
            while(!(pacman == end)) {
                path.push_back(pacman);
                pacman = parentMap[pacman];
            }
            printf("%lu\n", path.size() - 1);
            for (auto it = path.crbegin(); it != path.crend(); ++it) {
                printf("%s\n", it->toString());
            }
            break;
        }

        int offset[4][2] = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};
        for (int i = 0; i < 4; ++i) {
            Point neighbor(pacman.row + offset[i][0],
                           pacman.col + offset[i][1]);
            if (neighbor.row >= 0 && neighbor.row < nb_row &&
                neighbor.col >= 0 && neighbor.col < nb_col &&
                grid[neighbor.row][neighbor.col] != '%') {
                grid[neighbor.row][neighbor.col] = '%';
                parentMap[neighbor] = pacman;
                queue.push_back(neighbor);
            }
        }
        pacman = queue.front();
        queue.pop_front();
    }


    return 0;
}
